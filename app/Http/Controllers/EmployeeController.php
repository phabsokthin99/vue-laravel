<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    public function fetch_emp(){

        $emp = DB::table('employees')->get();
        return response()->json($emp);
    }

    //delete

    public function deleteEmp($id){
        // $employee->delete();
        // return response()->json([
        //     'message' => "Employee Delete success",

        // ]);

        $emp = Employee::where('id', $id)->first();
        $emp->delete();
        return response()->json([
            'message' => "delete employee success",
        ]);
    }

    public function saveEmployee(Request $request){

        $emp = new Employee();

        $emp->emp_name = $request->emp_name;
        $emp->age = $request->age;
        $emp->phone = $request->phone;
        $emp->save();

        return response()->json([
            'message' => "Save employee Success",
        ]);
    }

    public function fetchDataByID($id){

        // $emp = Employee::where('id', $id)->first();
        // return response()->json($emp);
        $emp = Employee::find($id);
        return response()->json($emp);
    }

    public function updateEmploye(Request $request, $id){
        $emp = Employee::where('id', $id)->first();
        $emp->emp_name = $request->emp_name;
        $emp->age = $request->age;
        $emp->phone = $request->phone;
        $emp->update();

        return response()->json([
            'message' => "update success",
        ]);
    }
}

import { createRouter, createWebHistory } from "vue-router";
import Home from '@/layout/Home.vue'
import UpdateEmployee from '@/layout/UpdateEmployee.vue'
import Test from  '@/layout/Test.vue';
import TestUpdate from '@/layout/TestUpate.vue';


const routes = [
    {
        path: "/",
        component: Home,
        name: "home"
    },
    {
        name: "emp",
        path: "/emp/:id",
        component: UpdateEmployee,
       // name: "emp"
    },
    {
        path: "/test",
        component: Test,
        name: "test",

    },
    {
        path: "/:pathMatch(.*)*",
        component: Test
    },
    {
        path: "/tupdate/:id",
        component: TestUpdate,
        name: "tupdate"
    }

]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router

import './bootstrap';

import { createApp } from 'vue';
import app from './layout/app.vue'
import router from './routes/Route';

createApp(app).use(router).mount('#app')

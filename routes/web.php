<?php

use App\Http\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('{path}', function(){
//     return view('app');
// })->where('path', '(.*)');

Route::get('{path}', function(){
    return view('app');
});
// Route::get('/{pathMatch}', function(){
//     return view('app');
// })->where('pathMatch', ".*");

Route::get('/api/employee', [EmployeeController::class, 'fetch_emp']);
Route::post('/api/saveemp', [EmployeeController::class, 'saveEmployee']);
Route::delete('/api/delete/{id}', [EmployeeController::class, 'deleteEmp']);
Route::get('/api/employee/{id}', [EmployeeController::class, 'fetchDataByID']);
Route::get('{path}/{id}', function(){
    return view('app');
});
Route::patch('/api/employee/{id}', [EmployeeController::class, 'updateEmploye']);

